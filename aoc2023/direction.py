class Direction(object):
    N = U = 0
    E = R = 1
    S = D = 2
    W = L = 3
    NE = 4
    SE = 5
    SW = 6
    NW = 7
    DIRS = [
        (0, -1),
        (1, 0),
        (0, 1),
        (-1, 0),
        (1, -1),
        (1, 1),
        (-1, 1),
        (-1, -1)
    ]
    
    @staticmethod
    def from_string(s):
        try:
            return ['N', 'E', 'S', 'W', 'NE', 'SE', 'SW', 'NW'].index(s)
        except ValueError:
            return ['U', 'R', 'D', 'L'].index(s)