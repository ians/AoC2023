from aoc2023.direction import Direction


class Marker(object):
    def __init__(self, pos, facing=Direction.N):
        self.pos = pos
        self.facing = facing
    def relative_pos(self, direction, units=1):
        delta = Direction.DIRS[direction]
        return (self.pos[0] + delta[0] * units, self.pos[1] + delta[1] * units)
    def move(self, direction=None, units=1):
        if direction is None:
            direction = self.facing
        self.pos = self.relative_pos(direction, units)
        return self.pos
    def turn_left(self):
        self.facing -= 1
        self.facing %= 4
    def turn_right(self):
        self.facing += 1
        self.facing %= 4
    def adjacent(self):
        return [self.relative_pos(d) for d in range(4)]
    def adjdiag(self):
        for dx, dy in Direction.DIRS:
            yield (self.pos[0] + dx, self.pos[1] + dy)
    def is_inside(self, field):
        x, y = self.pos
        return x >= 0 and x < field.width and y >= 0 and y < field.height
    def diff(self, marker):
        return (self.pos[0] - marker.pos[0], self.pos[1] - marker.pos[1])
    def copy(self):
        return Marker(self.pos, self.facing)
    def __str__(self):
        return f"Marker({self.pos}, {self.facing})"