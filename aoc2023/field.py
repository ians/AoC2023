import logging
import networkx as nx
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import copy
from aocd import get_data

from aoc2023.marker import Marker

class Field(object):
    def __init__(self, data, force_width=None):
        try:
            self.field = data.split('\n')
        except:
            self.field = data.decode('ascii').split('\n')
        if force_width:
            self.width = force_width
        else:
            self.width = len(self.field[0])        
        self.field = [ r + ' ' * (self.width - len(r)) for r in self.field ]
        self.field = [ list(r) for r in self.field ]
        self.height = len(self.field)
        self.markers = []
        
    def clone(self):
        f = Field('')
        f.field = copy.deepcopy(self.field)
        f.width = self.width
        f.height = self.height
        # TODO markers
        return f
    
    def dump(self):
        print(self.raw)
    
    @staticmethod
    def from_day(day):
        return Field(get_data(day))
    
    @staticmethod
    def from_numpy(arr):
        f = Field('')
        f.field = arr.tolist()
        f.width = len(f.field[0])
        f.height = len(f.field)
        return f
    
    @property
    def raw(self):
        return '\n'.join(''.join([str(c) for c in r]) for r in self.field)
        
    def add_marker(self, marker):
        self.markers.append(marker)
        
    def get(self, x, y, oob_none=False):
        if x < 0 or y < 0 or x >= self.width or y >= self.height:
            if oob_none:
                return None
            raise KeyError('(%d:%d): out of bounds! [%dx%d]' % (x, y, self.width, self.height))
        return self.field[y][x]
    
    def set(self, x, y, v):
        self.field[y][x] = v
    
    def find(self, v):
        for y in range(self.height):
            for x in range(self.width):
                if self.get(x, y) == v:
                    yield (x, y)

    def iterate(self):
        for y in range(self.height):
            for x in range(self.width):
                yield (x, y), self.get(x, y)
                
    def iterate_marker(self):
        for y in range(self.height):
            for x in range(self.width):
                yield Marker((x, y))
                                
    @property
    def numpy(self):
        return np.array(self.field)
    
    @property
    def df(self):
        return pd.DataFrame(self.numpy)
    
    def map(self, fn):
        clone = self.clone()
        for (x, y), v in clone.iterate():
            clone.set(x, y, fn(v))
        return clone
    
    def plot(self, key_fn=None):        
        if key_fn is None:
            mapping = { v: i for (i, v) in enumerate(np.unique(self.numpy)) }
            key_fn = lambda x: mapping[x]
            logging.info('Using default key_fn: %s', mapping)
        mapped = np.vectorize(key_fn)(self.numpy)
        if len(self.markers) > 0:
            marker_key = np.max(mapped) + 1
            for m in self.markers:
                mapped[m.pos[1]][m.pos[0]] = marker_key
                marker_key += 1            
        plt.imshow(mapped)
        
    def nx_maze(self, is_wall_fn=lambda t, pos: t == '#'):
        G = nx.grid_2d_graph(self.width, self.height)
        for pos, t in self.iterate():
            if is_wall_fn(t, pos):
                G.remove_node(pos)
        return G
    
    def foo(self):
        print("OKff")