import numpy as np
from matplotlib import pyplot as plt


def plot_xy_list(it):
    coords = np.array(list(it))
    origin = np.min(coords, axis=0)
    coords -= origin
    grid = np.full(np.max(coords, axis=0) + 1, 0)
    grid[tuple(coords.T)] = 1
    return plt.imshow(grid.T)